/**
 * @author Christian John Elmedo
 * @description WorkerThreadPool class, enables the use of threads
 * @2020
 */

const { EventEmitter } = require("events");
const { Worker, MessageChannel, MessagePort } = require("worker_threads");
const cpus = require("os").cpus().length;

class WorkerThreadsPool extends EventEmitter {
  /**
   * contructor
   * @param {string} fileName worker path
   * @param {number} numberOfChild number of worker threads
   * @param {object} opts worker options
   */
  constructor(fileName, numberOfChild = cpus, workerData = {}) {
    super();

    this.fileName = fileName;
    this.workerData = workerData;

    this.ports = [];
    this.workers = [];
    this.freeWorkers = [];

    for (let i = 0; i < numberOfChild; i++) this.addWorker();
  }

  addWorker(_workerData) {
    const { port1, port2 } = new MessageChannel();

    const obj = _workerData ? _workerData : this.workerData;
    const workerData = { port: port2, ...obj };
    const transferList = [port2];

    const worker = new Worker(this.fileName, { workerData, transferList });

    worker.on("error", () => {
      // Remove the worker from the list and start a new Worker to replace the
      // current one.
      this.workers.splice(this.workers.indexOf(worker), 1);
      this.ports.splice(this.ports.indexOf(port1), 1);
      this.addWorker(_workerData);
    });

    worker.on("message", (mssg) => {
      // In case of done, mark the worker as free
      if (mssg && mssg === "done") {
        this.freeWorkers.push(worker);
        this.emit("workerFree");
      }
    });

    this.workers.push(worker);
    this.freeWorkers.push(worker);
    this.ports.push(port1);
  }

  /**
   * close
   * @description terminate all worker threads
   */
  close() {
    for (const worker of this.workers)
      if (worker instanceof Worker) worker.terminate();
  }

  clearPending() {
    this.removeAllListeners("workerFree");
  }

  /**
   * runTask
   * @param {any} task task to send on specific thread
   */
  runTask(task) {
    if (this.freeWorkers.length === 0) {
      // No free worker, wait until a worker becomes free.
      this.once("workerFree", () => this.runTask(task));
      return;
    }
    const worker = this.freeWorkers.pop();
    if (worker instanceof Worker) worker.postMessage(task);
  }

  /**
   * portMessage
   * @param {any} mssg message for all threads via port
   */
  portMessage(mssg) {
    for (const port of this.ports)
      if (port instanceof MessagePort) port.postMessage(mssg);
  }
}

module.exports = WorkerThreadsPool;
