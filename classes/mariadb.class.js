const mariadb = require("mariadb");
const Logger = require("./logger.class");

/**
 * @author Christian John Elmedo
 * @description db connection class
 * @method constructor
 * @method connect
 * @method query
 * 11/19/2019
 */
class DBClass extends Logger {
  /**
   * constructor
   * @param {string} host db hostname
   * @param {integer} port db port
   * @param {string} user db user
   * @param {string} pass db password
   * @param {string} database target database name
   */
  constructor(host, port, user, pass, database) {
    super(`db://${host}:${port}/${database}`);
    this.host = host;
    this.port = port;
    this.user = user;
    this.pass = pass;
    this.database = database;
    this.connection = null;
    this.error = null;
    this.connected = false;
    this.attempts = 0;
  }

  /**
   * connect
   * @description call connect to create db connection
   * @param {object} opt db connection option
   * @returns {boolean} true if connect is success and false if there's an error
   */
  async connect(opt) {
    let self = this;
    try {
      // increment attempts
      self.attempts++;

      // opts
      const _opt = {
        host: self.host,
        port: self.port,
        user: self.user,
        password: self.pass,
        database: self.database,
        connectionLimit: 1,
      };

      // create connection
      let create = mariadb.createConnection(opt ? opt : _opt);

      // get connection
      let connection = await create;

      // event error listener
      connection.on("error", (error) => {
        self.logger.error("db connection error", error);
        self.connection = null;
        self.error = error;
        self.connected = false;
        setTimeout(() => {
          self.logger.debug("attempting to reconnect attempts:", self.attempts);
          self.connect();
        }, self.attempts * 100);
      });

      // check if connected
      (await connection.ping()) === null
        ? (self.connected = true)
        : (self.connected = false);

      if (self.connected) {
        self.logger.debug("db connection connected");
        self.connection = connection;
        self.error = null;
      }

      return true;
    } catch (error) {
      self.logger.error("db connect throw an error", error);

      self.connection = null;
      self.error = error;
      self.connected = false;
      // try to reconnect with timeout
      setTimeout(() => {
        self.logger.debug("attempting to reconnect attempts:", self.attempts);
        self.connect();
      }, self.attempts * 100);
      return false;
    }
  }

  /**
   * query
   * @param {string} query db query
   * @returns {any} return results
   * @throws {error} if theres a problem in the query of no db connection
   */
  async query(query, args) {
    try {
      if (this.connection) {
        let result = await this.connection.query(query, args);
        return result;
      }
    } catch (error) {
      this.logger.error("query throw an error", error);
      return null;
    }
  }

  /**
   * destroy
   * @description destroy db connection
   * @returns {boolean}
   */
  async destroy() {
    try {
      await this.connection.destroy();
      this.connected = false;
      return true;
    } catch (error) {
      this.logger.error("destroy thow an error", error);
      return;
    }
  }
}

module.exports = DBClass;
