const redis = require("redis");
const Logger = require("./logger.class");

/**
 * redis connection class
 * @author christian john elmedo
 * @2020
 */
class RedisClass extends Logger {
  /**
   * contructor
   * @param {string} host redis host name
   * @param {number} port redis port
   * @param {object} opt redis connection options
   */
  constructor(host, port, opt = {}) {
    super(`redis://${host}:${port}`);
    this.host = host;
    this.port = port;
    this.opt = opt;
    this.connected = false;
    this.connection = null;
    this.error = null;
  }

  retry_strategy(options) {
    if (options.error && options.error.code === "ECONNREFUSED") {
      // End reconnecting on a specific error and flush all commands with
      // a individual error
      return this.logger.error("The server refused the connection");
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
      // End reconnecting after a specific timeout and flush all commands
      // with a individual error
      return this.logger.error("Retry time exhausted");
    }
    if (options.attempt > 20) {
      // End reconnecting with built in error
      return undefined;
    }
    // reconnect after
    return Math.min(options.attempt * 100, 3000);
  }

  /**
   * connect
   * @description connect function, call to establish redis connection
   */
  connect() {
    try {
      if (!this.connected) {
        // create redis client
        this.connection = redis.createClient({
          ...this.opt,
          host: this.host,
          port: this.port,
          retry_strategy: this.retry_strategy,
        });

        this.connection.on("connect", () => {
          this.logger.debug("connection established");
          this.connected = true;
          this.error = null;
        });

        this.connection.on("error", (error) => {
          this.logger.error("connection error", error);
          this.connected = false;
          this.error = error;
        });

        this.connection.on("end", () => {
          this.logger.error("connection ended");
          this.connected = false;
        });
      }
    } catch (error) {
      this.logger.error(`connect throw an error`, error);
    }
  }

  /**
   * end
   * @description end function, call to end redis connection
   */
  end() {
    if (this.connection) {
      this.connection.end(true);
      this.connected = false;
      this.error = "connectiong ended manually";
    }
  }
}

module.exports = RedisClass;
