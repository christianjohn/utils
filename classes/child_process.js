/**
 * @author Christian John Elmedo
 * @description child process class. spawn new Node.js processes
 * @2020
 */

const { fork } = require("child_process");
const { EventEmitter } = require("events");
const cpus = require("os").cpus().length;

class ChildProcessPool extends EventEmitter {
  /**
   * contructor
   * @param {string} fileName worker filename | path
   * @param {number} numberOfChild number of worker to fork
   * @param {string} args argument to pass
   * @param {object} opts worker(child_process) options
   */
  constructor(fileName, numberOfChild = cpus, args = null, opts = {}) {
    super();

    this.fileName = fileName;
    this.opts = opts;
    this.args = args;

    this.workers = [];
    this.freeWorkers = [];

    for (let i = 0; i < numberOfChild; i++) this.addWorker();
  }

  addWorker(opts) {
    const obj = opts ? opts : this.opts;

    const worker = fork(this.fileName, this.args, { ...obj });

    worker.on("error", () => {
      // Remove the worker from the list and start a new Worker to replace the
      // current one.
      this.workers.splice(this.workers.indexOf(worker), 1);
      this.addWorker(opts);
    });

    worker.on("message", (mssg) => {
      // In case of done, mark the worker as free
      if (mssg && mssg === "done") {
        this.freeWorkers.push(worker);
        this.emit("workerFree");
      }
    });

    this.workers.push(worker);
    this.freeWorkers.push(worker);
  }

  close() {
    for (const worker of this.workers) worker.kill();
  }

  clearPending() {
    this.removeAllListeners("workerFree");
  }

  runTask(task) {
    if (this.freeWorkers.length === 0) {
      // No free worker, wait until a worker becomes free.
      this.once("workerFree", () => this.runTask(task));
      return;
    }
    const worker = this.freeWorkers.pop();
    worker.send(task);
  }
}

module.exports = ChildProcessPool;
