function logNow(logFunction, type, title, ...args) {
  let temp = {
    message: args
      .map((i) => {
        if (typeof i === "object") return JSON.stringify(i);
        else return i;
      })
      .join(", "),
  };

  logFunction(
    JSON.stringify({ datetime: new Date(), type, title, message: temp.message })
  );
}

class Logger {
  constructor(title) {
    this.title = `[${(title || " ").toUpperCase()}]`;
    this.logger = {
      log: logNow.bind(null, console.log, "LOG", this.title),
      info: logNow.bind(null, console.info, "INFO", this.title),
      debug: logNow.bind(null, console.debug, "DEBUG", this.title),
      error: logNow.bind(null, console.error, "ERROR", this.title),
      warn: logNow.bind(null, console.warn, "WARN", this.title),
      trace: logNow.bind(null, console.trace, "TRACE", this.title),
    };
  }
}

module.exports = Logger;
